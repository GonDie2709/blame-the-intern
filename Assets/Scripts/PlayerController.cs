﻿using UnityEngine;
using System.Collections;
using Utility;

public class PlayerController : Entity
{
    public bool isDead = false;
    public bool canMove = false;
    public bool isJumping = false;
    bool jumpRelease = true;
    float jumpPressDuration = 0f;
    float jumpPressedTime = 0f;
    float jumpPressLeeway = 0.1f;

    [Space]
    public float moveSpd = 5f;
    public float jumpStr = 40f;

    [Space]
    public float tractorForce = 2f;
    public Transform gunPoint;
    Object tractoring;

    [Space]
    public Animator chestAnim;
    public Animator legsAnim;

    [Space]
    public Transform armsTrans;
    public GameObject armLeft;
    public Transform mouseTrans;

    LineRenderer _lineRenderer;

    Animator _animator;

    public Transform gunSound;
    public Transform stepSound;

    void Start()
    {
        Init();
    }

    protected override void Init()
    {
        base.Init();
        _animator = GetComponent<Animator>();
        _lineRenderer = GetComponent<LineRenderer>();
    }

    protected override void OnFixedUpdate()
    {
        if (!canMove || isDead) return;

        #region Movement

        float xAccel = Input.GetAxisRaw("Horizontal");

        isMoving = Mathf.Abs(xAccel) > 0f ? true : false;
        velocity.x = xAccel * moveSpd;

        if (!isMoving)
            _rigidBody.velocity = new Vector2(0f, _rigidBody.velocity.y);

        if (!isJumping && !isFalling)
        {
            string anim;
            if (facingRight)
            {
                if (xAccel > 0f) anim = "isWalking";
                else anim = "isWalkingBack";
            }
            else
            {
                if (xAccel > 0f) anim = "isWalkingBack";
                else anim = "isWalking";
            }

            if (anim == "isWalking") legsAnim.SetBool("isWalkingBack", false);
            else legsAnim.SetBool("isWalking", false);

            legsAnim.SetBool(anim, isMoving);
        }
        else if (isFalling || isJumping)
        {
            legsAnim.SetBool("isFalling", true);
            legsAnim.SetBool("isWalkingBack", false);
            legsAnim.SetBool("isWalking", false);
        }
        #endregion

        #region Jump

        if (Input.GetButtonUp("Jump"))
        {
            velForce.y /= 1.2f;
            isJumping = false;
            jumpRelease = true;
        }

        if (Input.GetButtonDown("Jump") && !isJumping)
        {
            jumpPressedTime = Time.time;
        }

        bool toJump = Input.GetButton("Jump");
        if (toJump && jumpRelease)
        {
            if (isGrounded && Time.time - jumpPressedTime < jumpPressLeeway && !isJumping)
            {
                isJumping = true;
                Jump();
                jumpPressedTime = 0f;
            }
            else if (isJumping)
            {
                jumpPressDuration += Time.deltaTime;

                if (jumpPressDuration < 0.2f)
                {
                    Jump();
                }
                else
                    jumpRelease = false;
            }
        }

        #endregion

        #region MouseLook

        Vector3 mouseAt = Input.mousePosition;
        Vector2 object_pos = Camera.main.WorldToScreenPoint(transform.position);

        mouseAt.x = mouseAt.x - object_pos.x;
        mouseAt.y = mouseAt.y - object_pos.y;

        float angle = Mathf.Atan2(mouseAt.y, mouseAt.x) * Mathf.Rad2Deg;
        float gunAngle = angle;

        if (angle >= -128 && angle <= -37)
        {
            armLeft.SetActive(false);
        }
        else
            armLeft.SetActive(true);

        ResetAnims();
        if (angle >= -128 && angle <= -37)
            chestAnim.SetBool("LookingDown", true);
        else if (angle >= 40 && angle <= 145)
            chestAnim.SetBool("LookingUp", true);
        else
            chestAnim.SetBool("Straight", true);

        if (angle > -81 && angle < 75)
            facingRight = true;
        else
        {
            angle = (angle + 180) * -1;
            facingRight = false;
        }

        armsTrans.rotation = Quaternion.Slerp(armsTrans.rotation, Quaternion.Euler(0, 0, angle), 1f);
        gunPoint.rotation = Quaternion.Slerp(gunPoint.rotation, Quaternion.Euler(0, 0, gunAngle), 1f);

        #endregion

        #region TractorGun

        if (Input.GetButton("Fire1"))
        {
            StartTractor(Utility.TractorType.Pull);
        }
        else if (Input.GetButton("Fire2"))
            StartTractor(Utility.TractorType.Push);
        else if (Input.GetButtonUp("Fire1") || Input.GetButtonUp("Fire2"))
        {
            if (tractoring) tractoring.StopTraction();
            tractoring = null;
        }

        if ((!Input.GetButton("Fire1") && !Input.GetButton("Fire2")))
        {
            gunSound.gameObject.SetActive(false);
            _lineRenderer.SetVertexCount(1);
        }

        #endregion
    }

    void Jump()
    {
        isGrounded = false;
        velForce.y += jumpStr * Mathf.Abs(0.15f - jumpPressDuration);
    }

    void StartTractor(TractorType type)
    {
        if (tractoring)
        {
            tractoring.StopTraction();
            //tractoring = null;
        }

        _lineRenderer.SetVertexCount(1);
        gunSound.gameObject.SetActive(false);

        RaycastHit2D hitInfo = Physics2D.Raycast(gunPoint.position, gunPoint.right, 7f, Raylayers.noPlayer);

        if (hitInfo.collider != null)
        {
            Vector3 heading = (Vector3)hitInfo.point - gunPoint.position;
            float distance = heading.magnitude;

            RaycastHit2D checkHit = Physics2D.Raycast(gunPoint.position, gunPoint.right, distance, 1 << LayerMask.NameToLayer("GravityLight"));
            if (checkHit.collider == null)
            {
                Tractor(hitInfo.collider.gameObject, hitInfo.point, hitInfo.distance, type, gunPoint.right);
            }
            else
            {
                TractorType lightType = checkHit.collider.GetComponent<GravityLight>().lightType;
                if (lightType == TractorType.All || type == lightType)
                {
                    DrawLaser(type, checkHit.point);
                    /*if (tractoring)
                    {
                        tractoring.StopTraction();
                        tractoring = null;
                    }*/
                }
                else
                    Tractor(hitInfo.collider.gameObject, hitInfo.point, hitInfo.distance, type, gunPoint.right);
            }
        }
        /*else if (tractoring)
        {
            tractoring.StopTraction();
            tractoring = null;
        }*/
    }

    void Tractor(GameObject target, Vector2 targetPos, float distance, TractorType type, Vector3 direction)
    {
        if (!tractoring)
            tractoring = target.GetComponent<Object>();
        else
        {
            if (tractoring.gameObject != target)
            {
                tractoring.StopTraction();
                tractoring = null;
            }

            gunSound.gameObject.SetActive(true);
            gunSound.position = (tractoring ? tractoring.transform.position : Vector3.zero) + (Vector3.right * 0.5f) * (facingRight ? 1 : -1) + Vector3.up * 0.5f;

            DrawLaser(type, targetPos);

            if (distance < .5f && type == TractorType.Pull)
            {
                if (tractoring) tractoring.Float(_transform, direction);
            }
            else
            {
                if (tractoring) tractoring.StartTraction(direction, type, tractorForce);
            }
        }
    }

    void DrawLaser(TractorType type, Vector2 targetPos)
    {
        Color color = type == TractorType.Pull ? Color.red : Color.blue;
        color.a = 0.2f;

        _lineRenderer.SetColors(color, color);
        _lineRenderer.SetVertexCount(2);
        _lineRenderer.SetPosition(0, gunPoint.position);
        _lineRenderer.SetPosition(1, targetPos);
    }

    public void Dead()
    {
        isDead = true;
        StopMovement(true);
        _transform.FindChild("Body").gameObject.SetActive(false);
        _animator.SetBool("isDead", true);
    }

    protected override void OnGrounded()
    {
        jumpPressedTime = 0f;
        jumpPressDuration = 0f;

        legsAnim.SetBool("isFalling", false);

        stepSound.gameObject.SetActive(true);
    }

    protected override void OnHeadTouch()
    {
        base.OnHeadTouch();

        isJumping = false;
        jumpPressedTime = 0f;
        jumpPressDuration = 0f;
    }

    void LateUpdate()
    {
        Vector3 localScale = _transform.localScale;

        if (facingRight)
            localScale.x = 1;
        else
            localScale.x = -1;

        _transform.localScale = localScale;
    }

    void ResetAnims()
    {
        chestAnim.SetBool("LookingDown", false);
        chestAnim.SetBool("LookingUp", false);
        chestAnim.SetBool("Straight", false);
    }
}