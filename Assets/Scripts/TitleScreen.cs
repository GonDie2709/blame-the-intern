﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour
{
    bool loading = false;
    public Rigidbody2D rigid;

    void Start()
    {
        FadeScreen.instance.SetFadeTime(0.1f);
        FadeScreen.instance.FadeStart();

        rigid.AddForce(Vector2.one * 800f);
        rigid.AddTorque(300f);
    }

	void Update () 
	{
        if (loading) return;

	    if(Input.GetKeyDown(KeyCode.Space))
        {
            loading = true;
            FadeScreen.instance.FadeStart(GameStart);
        }
	}

    void GameStart()
    {
        SceneManager.LoadScene("MainGame");
    }
}