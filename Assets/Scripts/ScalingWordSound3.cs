﻿using UnityEngine;
using System.Collections;

public class ScalingWordSound3 : MonoBehaviour
{
    Transform _transform;

    public float duration;

    private float time;

    float startRange = 0.3f;
    float endRange = 0.4f;

    void Start()
    {
        _transform = GetComponent<Transform>();
    }
		
	void Update () 
	{
        time += Time.deltaTime;
        
        float oscilationRange = (endRange - startRange) / 2;
        float oscilationOffset = oscilationRange + startRange;

        float result = oscilationOffset + Mathf.Sin(Time.time) * oscilationRange;

        _transform.localScale = new Vector3(result, result, 0);
    }
}