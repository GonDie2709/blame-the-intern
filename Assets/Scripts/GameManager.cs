﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class GameManager : Singleton<GameManager>
{
    public PlayerController player;

    public float screenScrollTime = 5f;

    static Vector3 checkPoint, screenPosition;

    bool gameStarted = false;

    Animator closingDoor;

    void Start()
    {
        if (checkPoint != Vector3.zero)
        {
            player.transform.position = checkPoint;
            Camera.main.transform.position = screenPosition;
        }

        Invoke("GameStart", 1f);
    }

    void Update()
    {
        if(Input.GetButtonDown("Restart") && gameStarted)
        {
            RestartGame();
        }
    }

    public void ScrollScreen(int scrollDist, float cameraOffset, Animator toClose)
    {
        closingDoor = toClose;
        Vector2 playerPos = player.transform.position;
        playerPos.x += 3f;

        player.MoveTo(playerPos, screenScrollTime);

        Vector3 scrollTo = new Vector3((Camera.main.orthographicSize * Screen.width / Screen.height) * (2f * scrollDist) + cameraOffset, Camera.main.transform.position.y, Camera.main.transform.position.z);
        Camera.main.GetComponent<ScrollCamera>().ScrollTo(scrollTo, screenScrollTime);

        checkPoint = playerPos;
        screenPosition = scrollTo;
    }

    public void ScrollEnd()
    {
        closingDoor.SetTrigger("Close");
        player.enablePhysics = true;
        player.canMove = true;
    }

    public void KillPlayer()
    {
        player.Dead();
        Invoke("RestartGame", 2f);
    }

    void GameStart()
    {
        FadeScreen.instance.FadeStart(FreePlayer);
    }

    void FreePlayer()
    {
        player.canMove = true;
        gameStarted = true;
    }

    public void RestartGame()
    {
        FadeScreen.instance.FadeStart(LoadGame);
    }

    void LoadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void FinalScreen()
    {
        checkPoint = Vector3.zero;
        screenPosition = Vector3.zero;

        FadeScreen.instance.FadeStart(CallCredits);
    }

    void CallCredits()
    {
        SceneManager.LoadScene("Credits");
    }
}