﻿using UnityEngine;
using System.Collections;
using Utility;

public class Spawner : MonoBehaviour
{
    Transform _transform;

    public float maxBoxes;
    float boxCount;
    public bool spawnOnTime = false;
    public float spawnTime;
    float timer, timer2;

    public float cannonPower = 8000f;

    public bool wildRotation = false;
    public float rotationTime;
    public float rotationMagnitude;

    public Transform spawnPoint;
    public GameObject[] boxes = new GameObject[5];
    public Transform[] waypoints = new Transform[2];

    public Transform boomSound;

    bool movingRight = false;
    bool movingLeft = false;

    void Start()
    {
        _transform = GetComponent<Transform>();
    }

    void Update()
    {
        if (boxCount >= maxBoxes || !spawnOnTime) return;

        timer += Time.deltaTime;
        timer2 += Time.deltaTime;

        float phase = Mathf.Sin(timer / rotationTime);

        _transform.localRotation = Quaternion.Euler(0, 0, 360 + phase * rotationMagnitude);

        if (timer2 >= spawnTime)
        {
            SpawnBox();
            timer2 = 0f;
        }
    }

    public void SpawnBox()
    {
        boxCount++;
        if (boxCount >= maxBoxes) return;

        boomSound.gameObject.SetActive(true);

        int rng = Random.Range(0, boxes.Length - 1);
        GameObject obj = Instantiate(boxes[rng]);

        obj.transform.position = _transform.position;
        Rigidbody2D rigidBody = obj.GetComponent<Rigidbody2D>();
        if (rigidBody) rigidBody.AddForce(-_transform.up * 8000f);
    }

    public void MoveCanonRight()
    {
        if (movingLeft) return;

        movingRight = true;
        Vector3 position = _transform.position;

        position += Vector3.right * (5f * Time.fixedDeltaTime);

        if (position.x <= waypoints[0].position.x) position.x = waypoints[0].position.x;
        else if (position.x >= waypoints[1].position.x) position.x = waypoints[1].position.x;

        _transform.position = position;
    }

    public void MoveCanonLeft()
    {
        if (movingRight) return;

        movingLeft = true;
        Vector3 position = _transform.position;

        position += Vector3.right * (-5f * Time.fixedDeltaTime);

        if (position.x <= waypoints[0].position.x) position.x = waypoints[0].position.x;
        else if (position.x >= waypoints[1].position.x) position.x = waypoints[1].position.x;

        _transform.position = position;
    }

    public void StopMovement()
    {
        movingRight = false;
        movingLeft = false;
    }

    public void RemoveBox()
    {
        boxCount--;
    }
}