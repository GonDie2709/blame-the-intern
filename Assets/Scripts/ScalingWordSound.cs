﻿using UnityEngine;
using System.Collections;

public class ScalingWordSound : MonoBehaviour
{
    Transform _transform;
    public Transform player;
    Collider2D playerCol;

    public float duration;

    private float time;

    void Start()
    {
        _transform = GetComponent<Transform>();
        playerCol = player.GetComponent<Collider2D>();
    }

    void OnEnable()
    {
        if (_transform)
        {
            _transform.SetParent(null);
            _transform.position = new Vector3(player.position.x, playerCol.bounds.min.y - 0.2f, 0f);
        }
        time = 0f;
    }
		
	void Update () 
	{
        time += Time.deltaTime;

        float num = 0.7f * (time / duration);
        _transform.localScale = new Vector3(num, num, 0);

        if (time > duration)
        {
            _transform.SetParent(player);
            gameObject.SetActive(false);
        }
    }
}