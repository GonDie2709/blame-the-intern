﻿using UnityEngine;
using System.Collections;

public class MouseController : MonoBehaviour
{
    Transform _transform;
    SpriteRenderer _renderer;

    Color mouseColor;
    bool isTouching = false;

    void Start()
    {
        Cursor.visible = false;

        _transform = GetComponent<Transform>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = _transform.position.z;

        _transform.position = mousePos;

        mouseColor = new Color(1f, 1f, 1f, 0.5f);

        if (Input.GetButton("Fire1"))
        {
            mouseColor = new Color(1f, 0f, 0f, 0.5f);
        }

        if (Input.GetButton("Fire2"))
        {
            mouseColor = new Color(0f, 0f, 1f, 0.5f);
        }

        if (isTouching) mouseColor.a = 1f;
    }
    
    void LateUpdate()
    {
        _renderer.color = mouseColor;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Object") || col.CompareTag("Platform"))
        {
            isTouching = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Object") || col.CompareTag("Platform"))
        {
            isTouching = false;
        }
    }
}