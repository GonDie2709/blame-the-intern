﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Utility;

public class StaticPlatform : Object
{
    [SerializeField]
    Vector2 axisLock;
    public Polarized polarized;

    Transform playerTrans;

    public bool canMove = true;

    List<Transform> carrying = new List<Transform>();

    void Start () 
	{
        Init();
    }

    protected override void Init()
    {
        base.Init();

        playerTrans = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    protected override void OnFixedUpdate () 
	{
        for(int i = 0; i < carrying.Count; i++)
        {
            if (carrying[i].parent == _transform)
            {
                carrying[i].SetParent(null);
                carrying.RemoveAt(i);
            }
        }

        Carrying();
    }

    void Carrying()
    {
        float distance = Mathf.Abs(_transform.position.y - 0.5f);

        RaycastHit2D[] hitInfos = Physics2D.BoxCastAll(_transform.position, _collider.bounds.size - (Vector3.one * 0.1f), 0f, Vector2.up, distance, Raylayers.noPlayer);

        for (int i = 0; i < hitInfos.Length; i++)
        {
            if (hitInfos[i].collider != null)
            {
                carrying.Add(hitInfos[i].transform);
                hitInfos[i].transform.SetParent(_transform);
            }
        }

        RaycastHit2D hitInfo = Physics2D.BoxCast(_transform.position, _collider.bounds.size - (Vector3.one * 0.1f), 0f, Vector2.up, distance, 1 << LayerMask.NameToLayer("Player"));

        if (hitInfo.collider != null)
        {
            playerTrans.SetParent(_transform);
        }
        else
            if (playerTrans.parent == _transform) playerTrans.SetParent(null);

    }

    public override void StartTraction(Vector3 direction, TractorType push, float tractorForce)
    {
        if (!canMove) return;

        gameObject.layer = 19;

        Vector3 vel = Vector3.zero;
        Vector3 newDirection = new Vector3(direction.x * axisLock.x, direction.y * axisLock.y, direction.z);

        if (push == TractorType.Push)
        {
            if (polarized != Polarized.Blue)
                vel = (newDirection * tractorForce) * Time.fixedDeltaTime;
        }
        else
        {
            if (polarized != Polarized.Red)
               vel = ((newDirection * -1) * tractorForce) * Time.fixedDeltaTime;
        }

        RaycastHit2D hitInfo = Physics2D.Raycast(_transform.position, vel, _collider.bounds.extents.x + 0.05f, (Raylayers.noTractor | 1 << LayerMask.NameToLayer("NormalCollisions")));

        if (hitInfo.collider != null)
        {
            vel = Vector3.zero;
        }

        _transform.position += vel;
    }

    public override void StopTraction()
    {
        gameObject.layer = 8;
    }
}