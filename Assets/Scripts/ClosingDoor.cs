﻿using UnityEngine;
using System.Collections;

public class ClosingDoor : MonoBehaviour
{
    public Transform doorSound;

    void DoorSound()
    {
        doorSound.gameObject.SetActive(true);
    }

    void StopDoorSound()
    {
        doorSound.gameObject.SetActive(false);
    }
}