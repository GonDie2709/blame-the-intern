﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovingPlatform : MonoBehaviour
{
    Transform _transform;
    Collider2D _collider;

    public bool isActive = false;
    public float speed = 10;
    public float waitTime = 0f;
    public Transform[] nodes;

    int switchCount;
    public int switchTotal;

    Transform playerTrans;
    bool waiting = false;
    float time;

    List<Transform> carrying = new List<Transform>();

    int currentNode = 0;
    int NextNode
    {
        get
        {
            waiting = true;
            int next = currentNode + 1;
            return (next >= nodes.Length ? 0 : next);
        }
    }

    void Start()
    {
        _transform = GetComponent<Transform>();
        _collider = GetComponent<Collider2D>();

        playerTrans = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    void FixedUpdate()
    {
        if (switchCount < switchTotal) return;

        for (int i = 0; i < carrying.Count; i++)
        {
            if (carrying[i].parent == _transform)
            {
                carrying[i].SetParent(null);
                carrying.RemoveAt(i);
            }
        }

        CheckPlayer();

        if (waiting)
        {
            time += Time.fixedDeltaTime;
            if(time >= waitTime)
            {
                waiting = false;
                time = 0f;
            }
        }
        else
        {
            Vector2 velocity = (nodes[currentNode].position - _transform.position).normalized * speed * Time.deltaTime;

            if ((_transform.position - nodes[currentNode].position).magnitude <= speed * Time.fixedDeltaTime)
            {
                velocity = Vector2.ClampMagnitude(velocity, (nodes[currentNode].position - _transform.position).magnitude);
                currentNode = NextNode;
            }

            _transform.Translate(velocity);
        }
    }

    void CheckPlayer()
    {
        RaycastHit2D[] hitInfos = Physics2D.BoxCastAll(_transform.position, _collider.bounds.size - (Vector3.one * 0.1f), 0f, Vector2.up, 0.5f, Raylayers.noPlayer);

        for (int i = 0; i < hitInfos.Length; i++)
        {
            if (hitInfos[i].collider != null)
            {
                carrying.Add(hitInfos[i].transform);
                hitInfos[i].transform.SetParent(_transform);
            }
        }

        RaycastHit2D hitInfo = Physics2D.BoxCast(_transform.position, _collider.bounds.size - (Vector3.one * 0.1f), 0f, Vector2.up, 0.5f, 1 << LayerMask.NameToLayer("Player"));

        if (hitInfo.collider != null)
        {
            playerTrans.SetParent(_transform);
        }
        else
            if (playerTrans.parent == _transform) playerTrans.SetParent(null);
    }

    public void AddSwitch()
    {
        switchCount++;
    }

    public void RemoveSwitch()
    {
        switchCount--;
    }
}
