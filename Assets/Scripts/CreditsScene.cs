﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CreditsScene : MonoBehaviour
{
    void Start()
    {
        FadeScreen.instance.SetFadeTime(3f);
        FadeScreen.instance.FadeStart(Credits);
    }

    void Credits()
    {
        Invoke("FadeAgain", 5f);
    }

    void FadeAgain()
    {
        FadeScreen.instance.FadeStart(ReloadGame);
    }

    void ReloadGame()
    {
        SceneManager.LoadScene("TitleScreen");
    }
}