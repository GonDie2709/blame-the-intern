﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
    public Animator closingDoor;

    Animator _animator;
    public int scrollDist = 1;
    public float cameraOffset = 0f;
    public bool isUnlocked = false;

    public Transform doorSound;

    bool entered = false;

    void Start()
    {
        _animator = GetComponent<Animator>();

        if(isUnlocked)
        {
            _animator.SetBool("isUnlocked", true);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player") && isUnlocked && !entered)
        {
            entered = true;

            DoorSound();
            PlayerController player = col.GetComponent<PlayerController>();
            player.canMove = false;
            player.StopMovement(true);
            closingDoor.SetTrigger("Triggered");
            _animator.SetTrigger("Open");
        }
    }

    public void UnlockDoor()
    {
        isUnlocked = true;
        _animator.SetBool("isUnlocked", true);
        gameObject.layer = 13;
    }

    public void LockDoor()
    {
        isUnlocked = false;
        _animator.SetBool("isUnlocked", false);
        gameObject.layer = 10;
    }

    void ScrollScreen()
    {
        StopDoorSound();
        GameManager.instance.ScrollScreen(scrollDist, cameraOffset, closingDoor);
    }

    void DoorSound()
    {
        doorSound.gameObject.SetActive(true);
    }

    void StopDoorSound()
    {
        doorSound.gameObject.SetActive(false);
    }
}