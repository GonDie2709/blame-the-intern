﻿using UnityEngine;
using System.Collections;

public class ScalingWordSound2 : MonoBehaviour
{
    Transform _transform;

    public float duration;

    private float time;

    public float size = 0.6f;

    void Start()
    {
        _transform = GetComponent<Transform>();
    }

    void OnEnable()
    {
        time = 0f;
    }
		
	void Update () 
	{
        time += Time.deltaTime;
        
        float num = size * (time / duration);
        _transform.localScale = new Vector3(num, num, 0);

        if (time > duration)
        {
            gameObject.SetActive(false);
        }
    }
}