﻿using UnityEngine;
using System.Collections;

public class Tuto_S1 : MonoBehaviour
{
    Animator _animator;

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void PlayAnimation()
    {
        _animator.SetTrigger("Play");
    }

    void LockWindow()
    {
        gameObject.SetActive(false);
    }
}