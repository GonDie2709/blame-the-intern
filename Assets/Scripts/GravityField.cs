﻿using UnityEngine;
using System.Collections;

public class GravityField : MonoBehaviour
{
    public int direction = 0;
    public float force;

    public Sprite sprite1;
    public Sprite sprite2;

    Transform _transform;
    AreaEffector2D _effector;
    SpriteRenderer _renderer;

    int[] forceAngles = new int[4] { 90, -90, -90, 90 };
    int[] spriteAngles = new int[4] { 0, 90, 0, 90 };
    Vector2[] characterForce = new Vector2[] { Vector2.up, Vector2.right, Vector2.down, Vector2.left };
    Sprite[] sprites;

    void Start()
    {
        _transform = GetComponent<Transform>();
        _effector = GetComponent<AreaEffector2D>();
        _renderer = GetComponent<SpriteRenderer>();

        sprites = new Sprite[] { sprite1, sprite2, sprite2, sprite1 };

        ChangeDirection(direction);
    }

	void OnTriggerStay2D(Collider2D col)
    {
        if(col.CompareTag("Player"))
        {
            PlayerController player = col.GetComponent<PlayerController>();
            player.AddVelForce(force * characterForce[direction]);
        }
    }

    public void ChangeDirection(int dir)
    {
        direction = dir;

        _transform.rotation = Quaternion.Euler(Vector3.forward * spriteAngles[direction]);
        _effector.forceAngle = forceAngles[direction];
        _renderer.sprite = sprites[direction];
    }
}