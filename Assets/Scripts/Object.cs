﻿using UnityEngine;
using System.Collections;
using Utility;

public class Object : MonoBehaviour
{
    protected Transform _transform;
    protected Collider2D _collider;

    void Start()
    {
        Init();
    }

    protected virtual void Init()
    {
        _transform = GetComponent<Transform>();
        _collider = GetComponent<Collider2D>();
    }

    void FixedUpdate()
    {
        OnFixedUpdate();
    }

    protected virtual void OnFixedUpdate() { }
    public virtual void StartTraction(Vector3 direction, TractorType push, float tractorForce) { }
    public virtual void StopTraction() { }
    public virtual void Float(Transform playerTrans, Vector3 direction) { }
}