﻿using UnityEngine;
using System.Collections;
using Utility;

public class _Entity : MonoBehaviour
{
    public bool enablePhysics = true;

    protected Transform _transform;
    protected Collider2D _collider;
    protected Rigidbody2D _rigidBody;

    public Collisions collisions;

    float gravity = 0.5f;
    float maxfall = 30f;

    public bool facingRight = true;

    public Vector2 velocity;
    protected Vector2 velForce;

    public bool isMoving = false;
    public bool isGrounded = false;

    public bool isFalling = false;

    int horizontalRays = 8;
    int verticalRays = 6;

    float timer;

    protected virtual void Init()
    {
        _transform = GetComponent<Transform>();
        _collider = GetComponent<Collider2D>();
        _rigidBody = GetComponent<Rigidbody2D>();

        collisions = new Collisions();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        OnFixedUpdate();

        if (enablePhysics)
        {
            if (!isGrounded /*|| mPlatform != null*/)
            {
                if (velocity.y + velForce.y < 0f)
                {
                    timer += Time.deltaTime;
                    isFalling = true;
                }

                //velocity = new Vector2(velocity.x, Mathf.Max(velocity.y - gravity, -maxfall));
            }

            Rect box = new Rect(
                _collider.bounds.min.x,
                _collider.bounds.min.y,
                _collider.bounds.size.x,
                _collider.bounds.size.y
                );

            #region COLLISION DETECTION

            Vector2 startPoint;
            Vector2 endPoint;
            Vector2 origin;
            float distance;
            float lerpAmount;
            int indexUsed;
            bool connected;

            RaycastHit2D[] hitInfos;

            #region GROUND DETECTION

            if (isGrounded || isFalling)
            {
                startPoint = new Vector2(box.xMin + 0.05f, box.min.y);
                endPoint = new Vector2(box.xMax - 0.05f, box.min.y);
                hitInfos = new RaycastHit2D[verticalRays];

                distance = 0.1f + Mathf.Abs(velocity.y * Time.deltaTime);
                indexUsed = 0;
                connected = false;

                for (int i = 0; i < verticalRays; i++)
                {
                    lerpAmount = (float)i / (float)(verticalRays - 1);
                    origin = Vector2.Lerp(startPoint, endPoint, lerpAmount);

                    hitInfos[i] = Physics2D.Raycast(origin, Vector2.down, distance, Raylayers.downRay);
                    //Debug.DrawLine(origin, hitInfos[i].point, Color.red, 0.1f);
                    if (hitInfos[i].collider != null)
                    {
                        connected = true;
                        indexUsed = i;
                        break;
                        /*if (hitInfos[i].fraction < smallestFraction)
                        {
                            indexUsed = i;
                            smallestFraction = hitInfos[i].fraction;
                        }*/
                    }
                }

                if (connected)
                {
                    collisions.top = true;

                    if(!isGrounded && timer >= 0.02f)
                    {
                        OnGrounded();
                    }

                    isGrounded = true;
                    isFalling = false;

                    _transform.Translate(Vector2.down * (hitInfos[indexUsed].fraction * distance - 0.1f));
                    velocity = new Vector2(velocity.x, 0);

                    timer = 0f;
                    //OnGrounded();
                    /*mPlatform = hitInfos[indexUsed].collider.GetComponent<MovingPlatform>();
                    if (mPlatform != null) mPlatform.Board(this);*/
                }
                else
                {
                    collisions.top = false;
                    isGrounded = false;
                    /*if (mPlatform != null)
                    {
                        mPlatform.Unboard(this);
                        mPlatform = null;
                    }*/
                }
            }
            #endregion

            #region HEAD DETECTION

            if (!isGrounded || isFalling)
            {
                int lastConnection = 0;

                startPoint = new Vector2(box.xMin + 0.05f, box.max.y);
                endPoint = new Vector2(box.xMax - 0.05f, box.max.y);
                hitInfos = new RaycastHit2D[verticalRays];

                distance = /*box.height / 2*/0.2f;// + Mathf.Abs(velocity.y * Time.deltaTime);
                indexUsed = 0;

                connected = false;

                for (int i = 0; i < verticalRays; i++)
                {
                    lerpAmount = (float)i / (float)(verticalRays - 1);
                    origin = Vector2.Lerp(startPoint, endPoint, lerpAmount);

                    hitInfos[i] = Physics2D.Raycast(origin, Vector2.up, distance, Raylayers.upRay);
                    //Debug.DrawLine(origin, hitInfos[i].point, Color.red, 0.1f);
                    if (hitInfos[i].fraction > 0)
                    {
                        connected = true;
                        lastConnection = i;
                    }
                }

                if (connected)
                {
                    OnHeadTouch();
                    collisions.top = true;
                    velForce = new Vector2(velForce.x, 0f);
                    _transform.Translate(Vector2.up * (hitInfos[lastConnection].fraction * distance - 0.2f));
                }
                else
                    collisions.top = false;

            }
            #endregion

            #region LATERAL DETECTION

            startPoint = new Vector2(box.center.x, box.yMin + 0.001f);
            endPoint = new Vector2(box.center.x, box.yMax - 0.001f);

            float sideRayLength = box.width / 2 + Mathf.Abs(velocity.x * Time.deltaTime) + 0.05f;
            Vector2 direction = Vector2.right;

            hitInfos = new RaycastHit2D[horizontalRays];

            // RIGHT DETECTION
            for (int i = 0; i < horizontalRays; i++)
            {
                lerpAmount = (float)i / (float)(horizontalRays - 1);
                origin = Vector2.Lerp(startPoint, endPoint, lerpAmount);

                hitInfos[i] = Physics2D.Raycast(origin, direction, sideRayLength, 1 << LayerMask.NameToLayer("Objects"));

                if (hitInfos[i].fraction > 0)
                {
                    collisions.right = true;
                    if (velocity.x >= 0)
                    {
                        //_transform.position = new Vector2(hitInfos[i].collider.bounds.min.x - (box.width / 2) - 0.05f, _transform.position.y);
                        velocity = new Vector2(0, velocity.y);
                        velForce = new Vector2(0, velForce.y);
                        break;
                    }
                }
                else
                    collisions.right = false;
            }

            // LEFT DETECTION
            direction = Vector2.left;

            hitInfos = new RaycastHit2D[horizontalRays];

            for (int i = 0; i < horizontalRays; i++)
            {
                lerpAmount = (float)i / (float)(horizontalRays - 1);
                origin = Vector2.Lerp(startPoint, endPoint, lerpAmount);

                hitInfos[i] = Physics2D.Raycast(origin, direction, sideRayLength, 1 << LayerMask.NameToLayer("Objects"));

                if (hitInfos[i].fraction > 0)
                {
                    collisions.left = true;
                    if (velocity.x <= 0)
                    {
                        //_transform.position = new Vector2(hitInfos[i].collider.bounds.max.x + (box.width / 2) + 0.05f, _transform.position.y);
                        velocity = new Vector2(0, velocity.y);
                        velForce = new Vector2(0, velForce.y);
                        break;
                    }
                }
                else
                    collisions.left = false;
            }

            #endregion

            #endregion

            /*if(Mathf.Abs(velForce.x) > 0)
            {
                velForce.x += (velForce.x > 0 ? -1 : 1) * (gravity * 0.75f);
                if (velForce.y < 0) velForce.y = 0;
            }

            if (velForce.y > 0)
            {
                velForce.y -= gravity * 0.75f;
                if (velForce.y < 0) velForce.y = 0;

                velocity.y *= 0.85f;
            }*/

            //_transform.Translate((velocity + velForce) * Time.fixedDeltaTime);
            _rigidBody.velocity = _rigidBody.velocity + ((velocity + velForce) * Time.fixedDeltaTime);
        }
    }

    public void MoveTo(Vector2 moveTo, float time)
    {
        StartCoroutine(MoveEntity(moveTo, time));
    }

    IEnumerator MoveEntity(Vector2 moveTo, float moveTime)
    {
        float moveDuration = 0f;
        Vector2 oldPos = _transform.position;

        while(moveDuration < moveTime)
        {
            moveDuration += Time.deltaTime;
            _transform.position = Vector2.Lerp(oldPos, moveTo, moveDuration / moveTime);

            yield return null;
        }
    }

    public void AddVelForce(Vector2 force)
    {
        velForce += force;
    }

    public void StopMovement(bool stopPhysics)
    {
        if (stopPhysics) enablePhysics = false;

        velForce = Vector2.zero;
        velocity = Vector2.zero;
    }

    protected virtual void OnFixedUpdate() { }
    protected virtual void OnGrounded() { }
    protected virtual void OnHeadTouch() { }
}