﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SimpleDoor : MonoBehaviour
{
    Animator _animator;
    public bool isUnlocked = false;

    public Transform doorSound;

    void Start()
    {
        _animator = GetComponent<Animator>();

        if (isUnlocked)
        {
            _animator.SetBool("isUnlocked", true);
            
        }
    }

    

    public void UnlockDoor()
    {
        isUnlocked = true;
        _animator.SetBool("isUnlocked", true);
        Invoke("OpenDoor", 1f);
    }

    void OpenDoor()
    {
        _animator.SetTrigger("Open");
        DoorSound();
    }

    public void LockDoor()
    {
        isUnlocked = false;
        _animator.SetBool("isUnlocked", false);
        gameObject.layer = 10;
    }

    void ScrollScreen()
    {
        StopDoorSound();
        gameObject.layer = 13;
    }

    void DoorSound()
    {
        doorSound.gameObject.SetActive(true);
    }

    void StopDoorSound()
    {
        doorSound.gameObject.SetActive(false);
    }
}