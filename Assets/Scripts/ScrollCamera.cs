﻿using UnityEngine;
using System.Collections;

public class ScrollCamera : MonoBehaviour
{
    Transform _transform;

    Vector3 oldPos, scrollTo;
    float scrollDuration = 0f;
    float scrollTime = 0f;
    bool scrolling = false;

	void Start () 
	{
        _transform = GetComponent<Transform>();
	}
		
	void Update () 
	{
	    if(scrolling)
        {
            if (scrollDuration < scrollTime)
            {
                Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("ScreenBoundaries"), LayerMask.NameToLayer("Objects"), true);
                Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("ScreenBoundaries"), LayerMask.NameToLayer("Platform"), true);
                scrollDuration += Time.deltaTime;
                _transform.position = Vector3.Lerp(oldPos, scrollTo, scrollDuration / scrollTime);
            }
            else
            {
                Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("ScreenBoundaries"), LayerMask.NameToLayer("Objects"), false);
                Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("ScreenBoundaries"), LayerMask.NameToLayer("Platform"), false);
                scrolling = false;
                GameManager.instance.ScrollEnd();
            }
        }
	}

    public void ScrollTo(Vector3 position, float time)
    {
        oldPos = _transform.position;
        scrollTo = position;
        scrollTime = time;
        scrollDuration = 0f;
        scrolling = true;
    }
}