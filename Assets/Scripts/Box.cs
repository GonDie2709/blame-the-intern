﻿using UnityEngine;
using System.Collections;
using Utility;

public class Box : Object
{
    Rigidbody2D _rigidBody;

    public Polarized polarized;

    public bool canMove = true;
    public bool isFloating = false;

    void Start()
    {
        Init();
    }

    protected override void Init()
    {
        base.Init();

        _rigidBody = GetComponent<Rigidbody2D>();
    }

    protected override void OnFixedUpdate()
    {
        float distance = Mathf.Abs(_transform.position.y - 0.5f);

        RaycastHit2D hitInfo = Physics2D.BoxCast(_transform.position, _collider.bounds.size - (Vector3.one * 0.05f), 0f, Vector2.up, distance, 1 << LayerMask.NameToLayer("Player"));

        if (hitInfo.collider != null)
        {
            StopTraction();
            canMove = false;
        }
        else
            canMove = true;
    }

    public override void StartTraction(Vector3 direction, TractorType push, float tractorForce)
    {
        if (!canMove) return;

        gameObject.layer = 19;

        if (push == TractorType.Push)
        {
            if (polarized != Polarized.Blue)
                _rigidBody.velocity = direction * tractorForce;
        }
        else
        {
            if (polarized != Polarized.Red)
                _rigidBody.velocity = (direction * -1) * tractorForce;
        }

        RaycastHit2D hitInfo = Physics2D.Raycast(_transform.position, _rigidBody.velocity, _collider.bounds.extents.x + 0.1f, Raylayers.noTractor);
        
        if (hitInfo.collider != null)
        {
            _rigidBody.velocity = Vector2.zero;
        }
    }

    public override void StopTraction()
    {
        gameObject.layer = 8;

        //_transform.SetParent(null);
        //_transform.localScale = Vector3.one;

        _rigidBody.constraints = RigidbodyConstraints2D.None;


        _rigidBody.velocity = new Vector2(0f, _rigidBody.velocity.y);
    }

    public override void Float(Transform playerTrans, Vector3 direction)
    {
        if (!canMove || polarized == Polarized.Red) return;

        //_transform.SetParent(playerTrans);
        //_transform.localScale = Vector3.one;

        _rigidBody.velocity = Vector2.zero;
        _rigidBody.constraints = RigidbodyConstraints2D.FreezePosition;
    }
}