﻿using UnityEngine;
using System.Collections;
using Utility;

public class GravityLight : MonoBehaviour
{
    public TractorType lightType;
    public SpriteRenderer lightRenderer;
    public Color[] colors = new Color[3];

    public GameObject flashlight;

    Collider2D _collider;
    SpriteRenderer _renderer;

    void Start()
    {
        _collider = GetComponent<Collider2D>();
        _renderer = GetComponent<SpriteRenderer>();
        _renderer.color = colors[(int)lightType];
        lightRenderer.color = colors[(int)lightType];
    }

    public void TurnLightOn()
    {
        _collider.enabled = true;
        StopCoroutine("FadeLight");
        StartCoroutine("FadeLight", true);
    }

    public void TurnLightOff()
    {
        _collider.enabled = false;
        StopCoroutine("FadeLight");
        StartCoroutine("FadeLight", false);
    }

    IEnumerator FadeLight(bool on)
    {
        float timer = 0f;
        int fromAlpha = on ? 0 : 1;
        float toAlpha = on ? 1 : 0;

        while(timer <= 0.2f)
        {
            timer += Time.fixedDeltaTime;
            Color color = new Color(lightRenderer.color.r, lightRenderer.color.g, lightRenderer.color.b);
            color.a = Mathf.Lerp(fromAlpha, toAlpha, timer / 0.2f);

            lightRenderer.color = color;

            yield return null;
        }
    }
}