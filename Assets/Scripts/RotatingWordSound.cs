﻿using UnityEngine;
using System.Collections;

public class RotatingWordSound : MonoBehaviour
{
    Transform _transform;

    public float magnitude;
    public float duration;

    private float time;

	void Start () 
	{
        _transform = GetComponent<Transform>();
	}
		
	void Update () 
	{
        time += Time.deltaTime;

        float phase = Mathf.Sin(time / duration);

        _transform.localRotation = Quaternion.Euler(0, 0, phase * magnitude);
	}
}