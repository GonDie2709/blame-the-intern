﻿using UnityEngine;

public class Button : MonoBehaviour
{
    [SerializeField]
    Switch _switch;

    public bool specificObj = false;
    public string[] tags;
    public Transform soundTrans;

    Animator _animator;
    Collider2D _collider;

    bool stay = false;

    void Start()
    {
        _animator = GetComponent<Animator>();
        _collider = GetComponent<Collider2D>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (CountCollisions() == 1)
        {
            if (soundTrans) soundTrans.gameObject.SetActive(true);
        }
        else return;

        if (specificObj)
        {
            for (int i = 0; i < tags.Length; i++)
            {
                if (col.CompareTag(tags[i]))
                {
                    Activate();
                    break;
                }
            }
        }
        else
        {
            if(col.CompareTag("Player") || col.CompareTag("Object") || col.CompareTag("Platform"))
            {
                Activate();
            }
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (CountCollisions() > 0) return;

        stay = false;

        if (specificObj)
        {
            for (int i = 0; i < tags.Length; i++)
            {
                if (col.CompareTag(tags[i]))
                {
                    Deactivate();
                    break;
                }
            }
        }
        else
        {
            if (col.CompareTag("Player") || col.CompareTag("Object") || col.CompareTag("Platform"))
            {
                Deactivate();
            }
        }
    }

    /*void OnTriggerStay2D(Collider2D col)
    {
        if (CountCollisions() == 1)
        {
            stay = true;
            if (soundTrans && !stay) soundTrans.gameObject.SetActive(true);
        }
        else return;

        if (specificObj)
        {
            for (int i = 0; i < tags.Length; i++)
            {
                if (col.CompareTag(tags[i]))
                {
                    Activate();
                    break;
                }
            }
        }
        else
        {
            if (col.CompareTag("Player") || col.CompareTag("Object") || col.CompareTag("Platform"))
            {
                Stay();
            }
        }
    }*/

    int CountCollisions()
    {
        LayerMask dynamicObjs = Raylayers.dynamicObjs;
        return Physics2D.OverlapAreaAll(_collider.bounds.max, _collider.bounds.min, dynamicObjs).Length;
    }

    void Activate()
    {
        _animator.SetBool("isPressed", true);
        _switch.ActivateSwitch();
    }

    void Deactivate()
    {
        _animator.SetBool("isPressed", false);
        _switch.DeactivateSwitch();
    }

    /*void Stay()
    {
        _switch.OnStaySwitch();
    }*/
}