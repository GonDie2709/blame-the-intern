﻿using UnityEngine;
using System.Collections;

public class FinalDoor : MonoBehaviour
{
    Animator _animator;

    public Transform doorSound;

    bool entered = false;

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void UnlockDoor()
    {
        if (entered) return;

        entered = true;

        _animator.SetBool("isUnlocked", true);
        Invoke("EndGame", 10f);
    }

    public void EndGame()
    {
        FadeScreen.instance.SetFadeTime(5f);
        GameManager.instance.FinalScreen();
    }

    void DoorSound()
    {
        doorSound.gameObject.SetActive(true);
    }

    void StopDoorSound()
    {
        doorSound.gameObject.SetActive(false);
    }
}