﻿using UnityEngine;
using System.Collections;
using Utility;

public class RotatingPlatform : Object
{
    Rigidbody2D _rigidBody;

    public Polarized polarized;

    void Start()
    {
        Init();
    }

    protected override void Init()
    {
        base.Init();

        _rigidBody = GetComponent<Rigidbody2D>();
    }

    public override void StartTraction(Vector3 direction, TractorType push, float tractorForce)
    {
        _rigidBody.isKinematic = false;

        if (push == TractorType.Push)
        {
            if (polarized != Polarized.Blue)
                _rigidBody.velocity = direction * (tractorForce / 2) /** Time.fixedDeltaTime*/;
        }
        else
        {
            if (polarized != Polarized.Red)
                _rigidBody.velocity = (direction * -1) * (tractorForce / 2) /** Time.fixedDeltaTime*/;
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.collider.CompareTag("Object"))
        {
            _rigidBody.isKinematic = false;
        }
    }
}