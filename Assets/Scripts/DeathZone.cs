﻿using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour
{
    bool playerTouched = false;

	void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Player"))
        {
            if (!playerTouched)
            {
                playerTouched = true;
                GameManager.instance.KillPlayer();
            }
        }
    }
}