﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CallbackEvent : UnityEvent<string> { }

[System.Serializable]
public class Switch
{
    public bool isActive = false;

    [SerializeField]
    CallbackEvent OnActivate;

    [SerializeField]
    CallbackEvent OnDeactivate;

    /*[SerializeField]
    CallbackEvent OnStay;*/

    public void ActivateSwitch()
    {
        isActive = true;
        OnActivate.Invoke("");
    }

    public void DeactivateSwitch()
    {
        isActive = false;
        OnDeactivate.Invoke("");
    }

    /*public void OnStaySwitch()
    {
        isActive = true;
        OnStay.Invoke("");
    }*/
}