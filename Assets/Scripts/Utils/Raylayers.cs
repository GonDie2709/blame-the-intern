﻿using UnityEngine;
using System.Collections;

public class Raylayers
{
    public static readonly int onlyCollisions;
    public static readonly int upRay;
    public static readonly int downRay;
    public static readonly int dynamicObjs;
    public static readonly int noPlayer;
    public static readonly int noObjects;
    public static readonly int noPlatforms;
    public static readonly int noTractor;

    static Raylayers()
    {
        onlyCollisions = 1 << LayerMask.NameToLayer("NormalCollisions") |
                         1 << LayerMask.NameToLayer("SoftTop") |
                         1 << LayerMask.NameToLayer("SoftBottom") |
                         1 << LayerMask.NameToLayer("Objects") |
                         1 << LayerMask.NameToLayer("ScreenBoundaries") |
                         1 << LayerMask.NameToLayer("Platform") |
                         1 << LayerMask.NameToLayer("TractorTarget");

        upRay = 1 << LayerMask.NameToLayer("NormalCollisions") |
                1 << LayerMask.NameToLayer("SoftBottom") |
                1 << LayerMask.NameToLayer("Objects") |
                1 << LayerMask.NameToLayer("ScreenBoundaries") |
                1 << LayerMask.NameToLayer("Platform") |
                1 << LayerMask.NameToLayer("TractorTarget");

        downRay = 1 << LayerMask.NameToLayer("NormalCollisions") |
                  1 << LayerMask.NameToLayer("SoftTop") |
                  1 << LayerMask.NameToLayer("Objects") |
                  1 << LayerMask.NameToLayer("ScreenBoundaries") |
                  1 << LayerMask.NameToLayer("Platform") |
                  1 << LayerMask.NameToLayer("TractorTarget");

        dynamicObjs = 1 << LayerMask.NameToLayer("Objects") |
                      1 << LayerMask.NameToLayer("Player") |
                      1 << LayerMask.NameToLayer("Platform") |
                      1 << LayerMask.NameToLayer("TractorTarget");

        noTractor = 1 << LayerMask.NameToLayer("Objects") |
                      1 << LayerMask.NameToLayer("Player") |
                      1 << LayerMask.NameToLayer("Platform");

        noPlayer = 1 << LayerMask.NameToLayer("Objects") |
                      1 << LayerMask.NameToLayer("Platform") |
                      1 << LayerMask.NameToLayer("TractorTarget");

        noObjects = 1 << LayerMask.NameToLayer("Player") |
                      1 << LayerMask.NameToLayer("Platform");

        noPlatforms = 1 << LayerMask.NameToLayer("Objects") |
                      1 << LayerMask.NameToLayer("Player");


    }
}