﻿using UnityEngine;
using System.Collections;

namespace Utility
{
    [System.Serializable]
    public enum TractorType { Push, Pull, All }

    [System.Serializable]
    public enum Polarized { Normal, Red, Blue }

    public struct Collisions
    {
        public bool top;
        public bool bottom;
        public bool left;
        public bool right;
    }
}