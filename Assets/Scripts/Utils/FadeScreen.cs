﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class FadeScreen : MonoBehaviour {

    private float timeSince = 0f;
    public float endTime = 0f;

    private Color baseColor = Color.black;
    private Color newColor;
    public Color targetColor;
    private SpriteRenderer _renderer;

    public bool isFading = false;

    private static FadeScreen _instance;
    public static FadeScreen instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<FadeScreen>();
            return _instance;
        }
    }

    public delegate void CallBack();

    void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _renderer.color = baseColor;
    }

    public void FadeStart(CallBack callbackFunc = null)
    {
        isFading = true;
        timeSince = 0f;

        StartCoroutine(Fading(callbackFunc));
    }

    IEnumerator Fading(CallBack callbackFunc = null)
    {
        while (timeSince < endTime)
        {
            timeSince += Time.deltaTime;
            newColor = Color.Lerp(baseColor, targetColor, timeSince / endTime);
            _renderer.color = newColor;

            yield return null;
        }

        if(callbackFunc != null) callbackFunc();
        Color tColor = targetColor;
        targetColor = baseColor;
        baseColor = tColor;
        isFading = false;
    }

    public void SetFadeTime(float time)
    {
        endTime = time;
    }
}